<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $this->validate($request, [
        //     'login' => 'required|string|max:255|unique:users',
        //     'email' => 'required|string|email|max:255|unique:users',
        //     'password' => 'required|string|min:5|confirmed',
        // ]);
        // dd($request['login'], $request['email'], $request['password']);
        $user = User::create([
            'user_type' => $request['user-type'],
            'login' => $request['login'],
            'email' => $request['email'],
            'password' => bcrypt($request['password'])
        ]);
        // dd($user);
        return redirect()->route('login')->with('message', 'IT WORKS!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    
    public function executorAdminPage()
    {
        return view('executor-admin-page');
    }

    public function studentAdminPage()
    {
        return view('student-admin-page');
    }

    public function adminPage() {       
        return view(Auth()->user()->user_type . '-admin-page');
    }



    public function checkLoginAndEmailExist(Request $request)
    {
        // валидация
        $response = [
            'loginExists' => '',
            'emailExists' => ''
        ];
        // ищем пользователей с таким логином
        $userLogin = User::where('login', '=', $request->input('login'))->get();        

        // если пользователь существует, отвечаем true
        if (count($userLogin) === 0) {
            $response['loginExists'] = false;              
        } else {
            // return response()->json(['userExists'=> true]);
            $response['loginExists'] = true; 
        }

        // ищем пользователей с таким email
        $userEmail = User::where('email', '=', $request->input('email'))->get();

        if (count($userEmail) === 0) {
            $response['emailExists'] = false;   
        } else {            
            $response['emailExists'] = true; 
        }  

        return response()->json($response);        
    }
    
}
