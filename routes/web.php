<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// старотвый роут
Route::get('/', function () {
    return view('welcome');
})->name('main');;

//Route::get('/welcome2', function () {
//    return view('welcome2');
//})->name('welcome2');;

Route::resource('users', 'UserController');
// временные роуты для показа страниц
Route::get('/executor-admin-page', 'UserController@executorAdminPage')->name('executorAdminPage');
Route::get('/student-admin-page', 'UserController@studentAdminPage')->name('studentAdminPage');

Route::get('/testPage', function () {
    return view('test');
});




Route::get('/admin-page', 'UserController@adminPage')->name('adminPage');    

// Route::get('/register', 'UserController@registration')->name('register');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::post('/checkLoginAndEmailExist', 'UserController@checkLoginAndEmailExist')->name('checkLoginAndEmailExist');
