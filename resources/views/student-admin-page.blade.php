@extends('layouts.main')

@section('content')
<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
	<div class="container">
		<a class="navbar-brand" href="#">ТВОЙ ВУЗ</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>



		<!-- button "make-order-btn"-->
		<button class="btn align-middle btn-outline-secondary make-order-btn" type="button">
			Сделать заказ <i class="fas fa-plus-circle"></i>
		</button>

		<div class="collapse navbar-collapse" id="navbarResponsive">
			<ul class="navbar-nav ml-auto">
				<li class="nav-item active">
					<a class="nav-link" href="#">Личный кабинет
						<span class="sr-only"></span>
					</a>
				</li>
				<li class="nav-item dropdown">
	        		<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown1" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Настройки</a>
	        		<div class="dropdown-menu" aria-labelledby="navbarDropdown1">
	          			<a class="dropdown-item" href="#">Профиль</a>
	          			<a class="dropdown-item" href="#">Вывод денег</a>
	          			<a class="dropdown-item" href="#">Чёрный список</a>
	          			<div class="dropdown-divider"></div>
	          			<a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Выход</a>
	        		</div>
	        		<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
	      		</li>

			</ul>
		</div>
	</div>
</nav>

<!-- Page Content -->
<div class="main executor admin-panel">
	<div class="container">
		<div class="row">
			<div class="col-lg-3">
				<h2 class="my-4">{{ Auth::user()->login }}</h2>
				<div class="list-group">
					<a href="#" class="list-group-item">Мои заказы</a>
					<a href="#" class="list-group-item">Прочее</a>
				</div>
			</div>
			<!-- /.col-lg-3 -->
			<div class="col-lg-9">
				<div class="row">
					<div class="col-md-12">
						<h3 class="my-4">Активные заказы</h3>
					</div>
					<div class="col-md-12">
						<div class="card">
							<div class="card-body">
								<h4 class="card-title">
									<a href="#">Заказ 1</a>
								</h4>
								<h5>10,00 BYN</h5>
								<p class="card-text">Описание заказа!</p>
							</div>
							<div class="card-footer">
								<small class="text-muted">&#9733; &#9733; &#9733; &#9733; &#9734;</small>
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="card">
							<div class="card-body">
								<h4 class="card-title">
									<a href="#">Заказ 2</a>
								</h4>
								<h5>10,00 BYN</h5>
								<p class="card-text">Описание заказа!</p>
							</div>
							<div class="card-footer">
								<small class="text-muted">&#9733; &#9733; &#9733; &#9733; &#9734;</small>
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<h3 class="my-4">В работе</h3>
					</div>
					<div class="col-md-12">
						<div class="card">
							<div class="card-body">
								<h4 class="card-title">
									<a href="#">Заказ 3</a>
								</h4>
								<h5>10,00 BYN</h5>
								<p class="card-text">Описание заказа!</p>
							</div>
							<div class="card-footer">
								<small class="text-muted">&#9733; &#9733; &#9733; &#9733; &#9734;</small>
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="card">
							<div class="card-body">
								<h4 class="card-title">
									<a href="#">Заказ 4</a>
								</h4>
								<h5>10,00 BYN</h5>
								<p class="card-text">Описание заказа!</p>
							</div>
							<div class="card-footer">
								<small class="text-muted">&#9733; &#9733; &#9733; &#9733; &#9734;</small>
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<h3 class="my-4">Готовые работы</h3>
					</div>
					<div class="col-md-12">
						<p class="my-4">в данный момент готовых работ нет</p>
					</div>
				</div>
				<!-- /.row -->
			</div>
			<!-- /.col-lg-9 -->
		</div>
		<!-- /.row -->
	</div>
	<!-- /.container -->
</div>
<!--modal start-->
<!-- Модальное окно "make-order-modal" начало -->
<div class="modal fade make-order-modal">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
{{--header--}}
			<div class="modal-header">
				<h5 class="modal-title text-primary">Заполните форму заказа учебной работы:</h5>
			</div>
{{--body--}}
			<div class="modal-body">

				<div class="container-fluid make-order-box">


					<form class="make-order-form">
						<div class="row make-order-row justify-content-center">
							<div class="col-8">
								<div class="form-group">
									<span>Тема вашей работы:</span>
									<input type="text" class="form-control form-control-sm mt-2 make-order-input-theme"
										   placeholder="Введите название вашей работы">
								</div>


								<div class="form-group">
									<span>Выберите тип работы:</span>
									<select class="btn btn-sm border make-order-select-type form-control form-control-sm mt-2">
										<option selected>Выберите поле</option>
										<option value="name">Курсовая работа</option>
										<option value="age">Диплом</option>
										<option value="address">Решение задач</option>
									</select>
								</div>

								<div class="form-group">
									<span>Выберите предмет:</span>
									<select class="btn btn-sm border make-order-select-object form-control form-control-sm mt-2">
										<option selected>Выберите поле</option>
										<option value="name">Информатика</option>
										<option value="age">Геодезия</option>
										<option value="address">Тригонометрия</option>
									</select>
								</div>

								<div class="form-group">
									<span>Оставьте комментарии к заказу:</span>
									<textarea class="form-control form-control-sm mt-2 make-order-textarea-comment"
											  rows="3" placeholder="Напишите коментарий"></textarea>
								</div>


								<div class="form-item-wrapper from-to-inputs-list">
									<div class="row">
										<div class="col-7">
											<div class="form-group">
												<span>Дополнительные материалы:</span>
												<input type="file" class="form-control-file form-control-sm ">
											</div>
										</div>
										<div class="col-5">
											<div class="row">
												<span>Кол-во страниц*:</span>
												<div class="col-6">
													<div class="input-group mb-3 input-group-sm">
														<divs class="input-group-prepend">
															<span class="input-group-text  badge badge-primary font-weight-light">от</span>
														</divs>
														<input type="text" class="form-control" placeholder="12">
													</div>
												</div>
												<div class="col-6">
													<div class="input-group mb-3 input-group-sm">
														<div class="input-group-prepend">
															<span class="input-group-text badge badge-primary font-weight-light">до</span>
														</div>
														<input type="text" class="form-control" placeholder="15">
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-7">
											<div class="form-group">
												<span>Срок сдачи:</span>
												<input type="text" class="form-control form-control-sm mt-2 make-order-input-theme w-50" placeholder="Календарик">
											</div>
										</div>
										<div class="col-5">
											<div class="form-group">
												<span>Бюджет в рулбях:</span>
												<input type="text" class="form-control form-control-sm mt-2 make-order-input-theme w-50" placeholder="1.000р">
											</div>
										</div>
									</div>
								</div>


							</div>
						</div>
					</form>

				</div>
			</div>
{{--footer--}}
			<div class="modal-footer justify-content-md-center">
				<button type="button" class="btn btn-primary modal-default" >Сохранить</button>
			</div>
		</div>
	</div>
</div>
<!-- Модальное окно "make-order-modal" конец -->
<!--modal end-->
<!-- Footer -->
<footer class="py-5 bg-dark">
	<div class="container">
		<p class="m-0 text-center text-white">Copyright &copy; ТВОЙ ВУЗ 2019</p>
	</div>
	<!-- /.container -->
</footer>
<!--./footer-->
@endsection

@section('custom-js')
	<script type="text/javascript" src="{{ asset('js/make-order-model.js') }}"></script>
@endsection