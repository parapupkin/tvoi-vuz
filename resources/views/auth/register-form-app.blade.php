<div class="error-container d-none">
	<span class="mb-3 error"></span>
</div>
<input type="email" class="fadeIn third zero-raduis" name="email" placeholder="email" required data-error-message = "Введите корректный email">
<div class="error-container d-none">
	<span class="mb-3 error"></span>
</div>
<input type="text" class="fadeIn second zero-raduis" name="login" placeholder="логин" required minlength="5">
<div class="error-container d-none">
	<span class="mb-3 error"></span>
</div>
<input type="password" class="fadeIn third zero-raduis" name="password" placeholder="пароль" required minlength="5">
<div class="error-container d-none">
	<span class="mb-3 error"></span>
</div>
<input type="password" class="fadeIn third zero-raduis" name="password-repeat" placeholder="пароль еще раз" required minlength="5">
<input type="submit" class="fadeIn fourth zero-raduis" value="Регистрация">

<a class="return-main" href="{{ route('main') }}">Вернуться на главную</a>      