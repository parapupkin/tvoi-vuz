@extends('layouts.main')

@section('custom-css')
<link rel="stylesheet" href="{{ asset('css/login-form.css') }}">
@endsection

@section('content')
<div class="main container login full-height">
    <!-- <div class="container full-height"> -->
        <div class="row full-height align-items-center">
            <div class="col-md-12 login-wrapper">               
                <div class="wrapper fadeInDown zero-raduis">
                    <div id="formContent">                          
                        <div class="fadeIn first card-header">
                            <h2 class="my-5">Регистрация на сайте ТВОЙ ВУЗ</h2>
                        </div>
                        <div class="mb-3">
                            <span class="success d-none">
                                Вы успешно зарегистрированы
                            </span>
                        </div>  
                        <ul class="nav nav-tabs nav-fill" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="student-tab" data-toggle="tab" href="#student" role="tab" aria-controls="student" aria-selected="true">Как заказчик</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="executor-tab" data-toggle="tab" href="#executor" role="tab" aria-controls="executor" aria-selected="false">Как автор</a>
                            </li>                            
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="student" role="tabpanel" aria-labelledby="student-tab">
                                <!-- Login Form -->
                                <form method="POST" action="{{ route('users.store') }}" novalidate>
                                    @csrf
                                    <input type="text" id="user-type" class="fadeIn second zero-raduis" name="user-type" hidden value="student">
                                    @include('auth.register-form-app') 
                                </form>
                            </div>
                            <div class="tab-pane fade" id="executor" role="tabpanel" aria-labelledby="executor-tab">
                                <form method="POST" action="{{ route('users.store') }}" novalidate>
                                    @csrf
                                    <input type="text" class="fadeIn second zero-raduis" name="user-type" hidden value="executor">
                                    @include('auth.register-form-app') 
                                </form>
                            </div>                       
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <!-- </div> -->
</div>
@endsection

@section('custom-js')
<script type="text/javascript" src="{{ asset('js/validation.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/create_new_user.js') }}"></script>
@endsection
