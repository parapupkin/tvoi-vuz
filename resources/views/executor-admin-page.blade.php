@extends('layouts.main')

@section('content')
<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
	<div class="container">
		<a class="navbar-brand" href="#">ТВОЙ ВУЗ</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>



		<button class="btn align-middle btn-outline-secondary find-button" type="button">Найти заказ <i class="fas fa-search-plus"></i></button>
		<div class="collapse navbar-collapse" id="navbarResponsive">
			<ul class="navbar-nav ml-auto">
				<li class="nav-item active">
					<a class="nav-link" href="#">Личный кабинет
						<span class="sr-only">(current)</span>
					</a>
				</li>
				<li class="nav-item dropdown">
	        		<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown1" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Настройки</a>
	        		<div class="dropdown-menu" aria-labelledby="navbarDropdown1">
	          			<a class="dropdown-item" href="#">Профиль</a>
	          			<a class="dropdown-item" href="#">Вывод денег</a>
	          			<a class="dropdown-item" href="#">Чёрный список</a>
	          			<div class="dropdown-divider"></div>
	          			<a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Выход</a>
	        		</div>
	        		<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
	      		</li>

			</ul>
		</div>
	</div>
</nav>

<!-- Page Content -->
<div class="main executor admin-panel">
	<div class="container">
		<div class="row">
			<div class="col-lg-3">
				<h2 class="my-4">{{ Auth::user()->login }}</h2>
				<div class="list-group">
					<a href="#" class="list-group-item">Мои работы</a>
					<a href="#" class="list-group-item">Баланс</a>
				</div>
			</div>
			<!-- /.col-lg-3 -->
			<div class="col-lg-9">
				<div class="row">
					<div class="col-md-12">
						<h3 class="my-4">Мои ставки</h3>
					</div>
					<div class="col-md-12">
						<div class="card">
							<div class="card-body">
								<h4 class="card-title">
									<a href="#">Ставка 1</a>
								</h4>
								<h5>15,00 BYN</h5>
								<p class="card-text">Тема ставки!</p>
							</div>
							<div class="card-footer">
								<small class="text-muted">&#9733; &#9733; &#9733; &#9733; &#9734;</small>
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="card">
							<div class="card-body">
								<h4 class="card-title">
									<a href="#">Ставка 2</a>
								</h4>
								<h5>15,00 BYN</h5>
								<p class="card-text">Тема ставки!</p>
							</div>
							<div class="card-footer">
								<small class="text-muted">&#9733; &#9733; &#9733; &#9733; &#9734;</small>
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<h3 class="my-4">В работе</h3>
					</div>
					<div class="col-md-12">
						<div class="card">
							<div class="card-body">
								<h4 class="card-title">
									<a href="#">Рабочий проект 1</a>
								</h4>
								<h5>15,00 BYN</h5>
								<p class="card-text">Тема проекта!</p>
							</div>
							<div class="card-footer">
								<small class="text-muted">&#9733; &#9733; &#9733; &#9733; &#9734;</small>
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="card">
							<div class="card-body">
								<h4 class="card-title">
									<a href="#">Рабочий проект 2</a>
								</h4>
								<h5>15,00 BYN</h5>
								<p class="card-text">Тема проекта!</p>
							</div>
							<div class="card-footer">
								<small class="text-muted">&#9733; &#9733; &#9733; &#9733; &#9734;</small>
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<h3 class="my-4">Выполненные работы</h3>
					</div>
					<div class="col-md-12">
						<p class="my-4">в данный момент выполненных работ нет</p>
					</div>
				</div>
				<!-- /.row -->
			</div>
			<!-- /.col-lg-9 -->
		</div>
		<!-- /.row -->
	</div>
	<!-- /.container -->
</div>
<!-- Footer -->
<footer class="py-5 bg-dark">
	<div class="container">
		<p class="m-0 text-center text-white">Copyright &copy; ТВОЙ ВУЗ 2019</p>
	</div>
	<!-- /.container -->
</footer>
<!--./footer-->
@endsection

