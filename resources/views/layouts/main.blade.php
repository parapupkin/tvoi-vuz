<!DOCTYPE html>
<html lang="ru">
	<head>
		<meta content="text/html;charset=utf-8" http-equiv="Content-Type">
		<meta name="description" content="ОПИСАНИЕ КОНТЕНТА">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title>ТВОЙ ВУЗ</title>
		<link rel="icon" type="image/png" href="{{ asset('favicon.png') }}">
		<!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
		<!-- fa -->
		<!-- <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous"> -->
		<link rel="stylesheet" href="{{ asset('css/fontawesome-free-5.8.2-web/css/all.css') }}" crossorigin="anonymous">
		<!-- ./fa -->
		<!-- bootstrap -->
		<!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"> -->
		<link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
		<!-- ./bootstrap -->
		<link rel="stylesheet" href="{{ asset('css/main.css') }}">
		<link rel="stylesheet" href="{{ asset('css/add.css') }}">


		@yield('custom-css')
	</head>
	<body>

			
		@yield('content')

		<!-- jquery -->
		<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script> -->

		<script src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
		<!-- ./jquery -->
		<!-- js bootstrap -->
		<!-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script> -->
		<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script> -->
		<script src="{{ asset('js/popper.js') }}" crossorigin="anonymous"></script>
		<!-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script> -->
		<script src="{{ asset('js/bootstrap.min.js') }}" crossorigin="anonymous"></script>
		<!-- js bootstrap -->
		<!-- <script type="text/javascript" src="js/main.js"></script> -->
		@yield('custom-js')
	</body>
</html>