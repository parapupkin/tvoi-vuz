@extends('layouts.main')

@section('content')

<div class="main login">
    <div class="container">
        <div class="row">
            <div class="col-md-12 login-wrapper">               
                <div class="wrapper fadeInDown zero-raduis">
                    <div id="formContent">
                        <!-- Tabs Titles -->

                        <!-- Icon -->
                        <div class="fadeIn first card-header">
                            <!-- <img src="http://danielzawadzki.com/codepen/01/icon.svg" id="icon" alt="User Icon" /> -->
                            <h2 class="my-5">Вход в систему</h2>
                        </div>

                        <!-- Login Form -->
                        <form method="POST" action="#">
                                       
                                <span class="errors">Неверный логин или пароль</span>
                            
                            <input type="text" id="login" class="fadeIn second zero-raduis form-control{{ $errors->has('login') ? ' is-invalid' : '' }}" name="login" placeholder="логин" value="{{ old('login') }}">
                            <input type="password" id="password" class="fadeIn third zero-raduis" name="password" placeholder="пароль">
                            <!-- <div id="formFooter">
                                <a class="underlineHover" href="#">Забыли пароль?</a>
                            </div> -->
                            <input type="submit" class="fadeIn fourth zero-raduis" value="ВХОД">
                            <!-- <h2>У вас нет аккаунта?</h2>
                            <input type="button" class="fadeIn fourth zero-raduis pc" value="РЕГИСТРАЦИЯ"> -->
                        </form>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection