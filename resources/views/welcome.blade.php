@extends('layouts.main')

@section('custom-css')
<link rel="stylesheet" href="{{ asset('css/welcome.css') }}">
@endsection

@section('content')
<div class="flex-center position-ref full-height">    

    <div class="content">
        <div class="title m-b-md">
            ТВОЙ ВУЗ
        </div>
        <!-- <div class="links">
            <a href="{{route('studentAdminPage')}}">Посмотреть кабинет студента</a>
            <a href="{{route('executorAdminPage')}}">Посмотреть кабинет исполнителя</a>
        </div> -->
        <div class="links">
            <a href="{{route('register')}}">Зарегистрироваться</a>
            <a href="{{route('login')}}">Войти</a>
        </div>
    </div>
</div>
@endsection
    
