'use strict';

function checkValidity(elem) {
	let errorMessage = '';
	
	if (elem.dataset.errorMessage) {
		// если элемент имеет строгий шаблон, то сообщение об ошибке берем из дата-атрибута
		if (!elem.validity.valid) {			
			errorMessage = elem.dataset.errorMessage;					
		}			
	} else {
		if (elem.validity.tooShort) {
			errorMessage = `Длина менее ${elem.minLength} символов не допускается`;					
		}	

		if (elem.validity.tooLong) {
			errorMessage = `Длина более ${elem.maxLength} символов не допускается`;				
		}

		if (elem.validity.patternMismatch) {			
			errorMessage = errorMessage ? errorMessage + `, допускается только ввод букв, знаков дефис, апостроф и пробел` :
			 `Допускается только ввод букв, знаков дефис, апостроф и пробел`;				
		}
	}
	

	if (elem.validity.valueMissing) {
		errorMessage = 'Поле должно быть обязательно заполнено';				
	}

	if (!elem.validity.valid) {				
		showErrorMessage(elem, errorMessage);
		possibilitySendForm = false;
		console.log(`не прошел валидацию элемент ${elem.name}`);	
	} 
}

function showErrorMessage(checkedItem, errorMessage) {
	const itemError = checkedItem.previousElementSibling;
	if (itemError) {
		const spanErrorMessage = itemError.querySelector('.error');
		if (spanErrorMessage) {
			itemError.classList.remove('d-none');
			checkedItem.classList.add('invalid');
			spanErrorMessage.innerText = errorMessage;
		}		
	}	
}

function hideErrorMessage(checkedItem) {
	const itemError = checkedItem.previousElementSibling;
	if (itemError) {
		const spanErrorMessage = itemError.querySelector('.error');
		if (spanErrorMessage) {
			itemError.classList.add('d-none');
			checkedItem.classList.remove('invalid');
			spanErrorMessage.innerText = '';
		}		
	}	
}