'use strict';

const forms = document.querySelectorAll('.tab-content form');	  

let possibilitySendForm = true;

function validate(event) {
	event.preventDefault();	
    possibilitySendForm = true;
    
    const inputs = event.target.querySelectorAll('input');
    // выполняем валидацию полей
    for (const input of inputs) {
    	hideErrorMessage(input);
    	checkValidity(input);    	
    }

    const formData = new FormData(event.target);
	fetch('checkLoginAndEmailExist', {method: 'POST', body: formData})
        .then(res => {
        	if (res.status !== 200) {
        		throw new TypeError('Произошла ошибка на сервере при сохранении контрагента');
        	}	        		            
            return res.json();          
        })
        .then(data => {
        	// event.target.submit();
        	console.log(data);

    	
    		if (data.loginExists == true) {
        		showErrorMessage(event.target.querySelector('input[name="login"]'), 'Пользователь с таким логином уже существует');	
        		possibilitySendForm = false;        			
        	}
        	if (data.emailExists == true) {
        		showErrorMessage(event.target.querySelector('input[name="email"]'), 'Пользователь с таким email уже существует');
        		possibilitySendForm = false; 	        			
        	}
        	if (!checkPasswordMatch(event.target)) {
        		showErrorMessage(event.target.querySelector('input[name="password"]'), 'Введенные пароли не совпадают');	
        		event.target.querySelector('input[name="password-repeat"]').classList.add('invalid');
        		possibilitySendForm = false;         			
        	}
        	if (possibilitySendForm) {
				event.target.submit();
			}
        	
        })
        .catch(() => {	        	
        	// success.classList.remove('success');
        	// success.classList.remove('d-none');
        	// success.classList.add('error');
        	// success.innerText = 'Произошла ошибка на сервере при сохранении контрагента';
        });	
    // если все поля заполнены верно - отправляем данные на сервер и выводим сообщение пользователю
    
    	
}


for (const form of forms) {
	form.addEventListener('submit', validate);
}

function checkPasswordMatch(form) {
	if (form.querySelector('input[name="password"]').value !== form.querySelector('input[name="password-repeat"]').value) {
		return false;
	} else {
		return true;
	}
}

	  